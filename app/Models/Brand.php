<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Brand extends Model
{
    // свзяь один ко многим brand to product
      public function products(): HasMany
    {
        return $this->hasMany(Product::class);
    }

    //use HasFactory;
}
