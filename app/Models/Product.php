<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{  //возвращаем категорию выбранного товара


    /**
     * Связь «товар принадлежит» таблицы `products` с таблицей `categories`
     */
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Связь «товар принадлежит» таблицы `products` с таблицей `brand`
     */
    public function brands()
    {
        return $this->belongsTo(Brand::class);
    }

    use HasFactory;
}
