<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;


class CatalogController extends Controller
{
    public function index()
    {
        $roots = Category::where('parent_id', 0)->get();
        return view('catalog.index', compact('roots'));
    }

    public function category(string $slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        // получаем товары категории от модели
        $product = $category->products;
        return view('catalog.category', compact('category', 'product'));
    }

    public function brand(string $slug)
    {
        $brand = Brand::where('slug', $slug)->firstOrFail();
        // получаем товары бренда от модели
        $products = $brand->products;
        return view('catalog.brand', compact('brand', 'products'));
    }

    public function product(string $slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        // получаем от модели категорию и бренд товара
        $category = $product->category;
        $brand = $product->brands;
        return view('catalog.product', compact('product', 'category', 'brand'));


    }
}
