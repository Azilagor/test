@extends('layout.site')

@section('content')
    <h1>Товар: {{ $products->name }}</h1>
    <p>Цена: {{ number_format($products->price, 2, '.', '') }}</p>
    <p>
        Категория:
        <a href="{{ route('catalog.category', ['slug' => $products->category_slug]) }}">
            {{ $products->category_name }}
        </a>
    </p>
    <p>
        Бренд:
        <a href="{{ route('catalog.brand', ['slug' => $products->brand_slug]) }}">
            {{ $products->brand_name }}
        </a>
    </p>
    <p>{{ $products->content }}</p>
@endsection
