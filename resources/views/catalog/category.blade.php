@extends('layout.site')

@section('content')
    <h1>{{ $category->name }}</h1>


    <div class="row">
        @foreach ($product as $item)
            @include('catalog.part.product', ['product' => $product])
        @endforeach
    </div>
@endsection
